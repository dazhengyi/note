# Linux常用指令（Centos7）  

## 常用指令  

- #### 修改系统时间  

  - date -s  “2018-12-16 21:35:00”

- #### 修改系统时区

  - https://support.huaweicloud.com/usermanual-hecs/hecs_03_0055.html
    - 注意文档有错，指令中间有斜杠 ln -sf /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime

##  *项目相关*

#####     发布项目 ：

​             1.直接启动：java -jar xxxx.jar  （ctrl+c/v 或者 exit  会停止项目的运行）

​              2.后台启动：> nohup java -Dfile.encoding=UTF-8 -jar xxxx.jar &    （会直接给出启动项目的进程号）

#####     根据项目名查看  ***进程***  号：

​            1.根据项目名查询 : ps -ef |grep xxxx （第一个数字是进程号）

#####     根据  *进程*  号杀死进程

​            1. kill - 9  进程号

#####      根据  *端口*   号查项目进程号：

​              1.如：netstat -apn|grep 8080

##### 查看防火墙状态

​    systemctl status firewalld     （查询结果：active (running) 表示开启中， inactive (dead)关闭中）

##### 开启防火墙

  systemctl start firewalld

##### 关闭防火墙

   systemctl stop firewalld

##### CentOS7防火墙开放端口号  

如果你发现在CentOS 7上开放端口用iptables没效果(或者是sodino没找到正确的命令,传说Centos7     

下默认的防火墙是 Firewall，替代了之前的 iptables)…
 使用firewall-cmd开放端口则立即就生效了。
 见下操作：
 firewall-cmd --state //查看运行状态
 // 开放1024的端口
 firewall-cmd --add-port=1024/tcp --permanent
 // 重载生效刚才的端口设置
 firewall-cmd --reload

// 查看防火墙所有开放的端口

firewall-cmd --zone=public --list-ports

 firewall常用命令如下：
 常用命令介绍
 firewall-cmd --state ##查看防火墙状态，是否是running
 firewall-cmd --reload ##重新载入配置，比如添加规则之后，需要执行此命令
 firewall-cmd --get-zones ##列出支持的zone
 firewall-cmd --get-services ##列出支持的服务，在列表中的服务是放行的
 firewall-cmd --query-service ftp ##查看ftp服务是否支持，返回yes或者no
 firewall-cmd --add-service=ftp ##临时开放ftp服务
 firewall-cmd --add-service=ftp --permanent ##永久开放ftp服务
 firewall-cmd --remove-service=ftp --permanent ##永久移除ftp服务
 firewall-cmd --add-port=80/tcp --permanent ##永久添加80端口
 iptables -L -n ##查看规则，这个命令是和iptables的相同的
 man firewall-cmd ##查看帮助
————————————————
原文链接：https://blog.csdn.net/yw_1207/article/details/90746899

添加80端口的访问权限，这里添加后永久生效
        #firewall-cmd --zone=public --add-port=80/tcp --permanent    
        #firewall-cmd --reload
查看80端口访问权限情况
       #firewall-cmd --zone= public --query-port=80/tcp
关闭80访问权限
       #firewall-cmd --zone= public --remove-port=80/tcp --permanent





##### firewall的其他命令操作

启动： systemctl start firewalld
查看状态： systemctl status firewalld 
停止： systemctl disable firewalld
禁用： systemctl stop firewalld
启动服务：systemctl start firewalld.service
关闭服务：systemctl stop firewalld.service
重启服务：systemctl restart firewalld.service
服务的状态：systemctl status firewalld.service
在开机时启用一个服务：systemctl enable firewalld.service
在开机时禁用一个服务：systemctl disable firewalld.service
查看服务是否开机启动：systemctl is-enabled firewalld.service
查看已启动的服务列表：systemctl list-unit-files|grep enabled
查看启动失败的服务列表：systemctl --failed
查看版本： firewall-cmd --version
查看帮助： firewall-cmd --help
显示状态： firewall-cmd --state
查看所有打开的端口： firewall-cmd --zone=public --list-ports
更新防火墙规则： firewall-cmd --reload
查看区域信息:  firewall-cmd --get-active-zones
查看指定接口所属区域： firewall-cmd --get-zone-of-interface=eth0
拒绝所有包：firewall-cmd --panic-on
取消拒绝状态： firewall-cmd --panic-off
查看是否拒绝： firewall-cmd --query-panic
————————————————
原文链接：https://blog.csdn.net/lcyong_/article/details/78928223

# Docker

## 常用指令

###  copy  

- 从容器中copy到主机

  - ```js
    docker cp  96f7f14e99ab:/www/test /tmp/
    ```

    eg: 将容器中的test目录拷贝到主机的tmp目录下

  - ``` js
    docker cp  96f7f14e99ab:/www/test /tmp/test1
    ```

    eg: 将容器中的test目录拷贝到主机的tmp目录下并重命名为test1

- 从主机中copy到容器

  - ``` js
    docker cp /www/test 96f7f14e99ab:/www/
    ```

    eg: 将主机中的test目录copy到容器的www的目录下  

## 镜像指令

## 容器指令  

1. 从容器里面退出

   Ctrl+p+q

## 发布java项目  

### 基于java:8直接部署

​           1.1 docker pull java:8 

​           1.2 docker run -it  --name XXX(容器别名)  -p 8080:8080   
​                  -v /usr/project:/usr/project  java:8

​                  -p 设置映射端口号：前面的那个是外部端口，后面的那个是docker内部容器的端口

​                  -v  数据挂载目录：前面是宿主机绝对路径，后面的容器内的路径

​            1.3 将java项目的jar包放到宿主机的/usr/project下，进入java容器的/usr/project运行jar包

​                   nohub java -Dfile.encoding=UTF-8 -jar   xxx.jar &  

### 基于Dockerfile部署  

1. 打包项目

2. 上传项目到Linux，并在同一目录下编写Dockerfile  

   ``` java
   FROM java:8
   // VOLUME /tmp
   ADD advert-0.0.1-SNAPSHOT.jar app.jar 
   EXPOSE 8081
   ENTRYPOINT ["java","java -Dfile.encoding=UTF-8","-jar","/app.jar"]
     /*  FROM：表示基础镜像，即运行环境
   
       VOLUME：一个特别指定的目录，用于存储数据，该命令的作用是在/var/lib/docker创建一个名为tmp的目录，     在开启redis服务时，需要特别指定redis的数据存储在哪个文件夹，此时这个命令就十分有用。而在发布
       SpringBoot项目时：VOLUME指向了一个/tmp的目录，由于Spring Boot使用内置的Tomcat容器，Tomcat默 
       认使用/tmp作为工作目录。效果就是在主机的/var/lib/docker目录下创建了一个临时文件，并连接到容器
       的/tmp。这样日志就会打印到临时文件的_data目录下
   
       ADD：拷贝文件并且重命名
   
       EXPOSE：并不是真正的发布端口，这个只是容器部署人员与建立image的人员之间的交流，即建立image的人员告     诉容器布署人员容器应该映射哪个端口给外界
   
       ENTRYPOINT：容器启动时运行的命令，相当于我们在命令行中输入java -jar xxxx.jar
   
    */   
       
       
   ```

   

3. 构建项目：docker build  -t  XXX（项目取别名） .     （注意最后那个点  .   不能省略）    

4. 运行容器：docker run -it --name advert -p 8081:8081 -v /srv/www/java/logs/:/logs -d advert:6.0  

   ```java 
   -v /srv/www/java/logs/:/logs 在SpringBoot项目中，日志我设定的时打印在根路径下的logs文件夹中                                   file: /logs/advert.log，所以将/logs文件夹挂载。而如果                                       Dockerfile中写了VOLUME /tmp 就不需要挂载，去/var/lib/docker                               找日志文件就好了
   ```

   

## 创建Nginx容器

​            2.1 docker pull nginx   
​                   新建几个文件夹  /usr/project/nginx/html  
​                                                /usr/project/nginx/conf.d  
​                                                /usr/project/nginx/nginx.conf

​            2.2   先启动无挂载目录的nginx，copy配置文件到挂载目录，再启动有挂载目录的Nginx
​                  2.2.1 将nginx先启动docker run -it --name nginx-01 -p 80:80  -d nginx

​                  2.2.2 将容器下的default.conf 和 nginx.conf 拷贝到宿主机的对应文件夹下

                            docker cp    xxx(nginx-01的id)：/etc/nginx/conf.d/default.conf                                           /usr/project/nginx/conf.d/default.conf  
                            docker cp    xxx(nginx-01的id)：/etc/nginx/conf.d/default.conf                                           /usr/project/nginx/conf.d/default.conf
​                   2.2.3 启动有 挂载目录的Nginx 

``` js
    docker run -it --name nginx -p 80:80  
           -v /srv/www/nginx/html:/usr/share/nginx/html     
           -v /srv/www/nginx/conf.d/default.conf:/etc/nginx/conf.d/default.conf   
           -v /srv/www/nginx/nginx.conf/nginx.conf:/etc/nginx/nginx.conf -d nginx
```

​           2.3 发布前端项目    
​                  2.3.1 将前端的包dist解压 放到宿主机的html下  
​                  2.3.2  修改宿主机 /usr/project/nginx/conf.d下的default.conf文件 

## 创建redis容器

   1.docker pull redis:5.0

        2. 将redis的配置文件redis.conf放到 /var/redis/conf/redis.conf下面
           3. 启动：docker run -p 6379:6379 --name redis -v /var/redis/data:/data   
                         -v  /var/redis/conf/redis.conf:/etc/redis/redis.conf    
                         -d redis:5.0 redis-server /etc/redis/redis.conf/redis.conf --appendonly yes  
                         （注意以redis.conf启动redis的服务是/redis.conf文件夹下的redis.conf文件）

##  创建mysql容器

   1. docker pull mysql:5.7

   2. 创建三个文件夹/var/mysql/conf，/var/mysql/logs，/var/mysql/data

   3. ``` java
      docker run -p 3306:3306 --name mysql 
            -v /var/mysql/conf:/etc/mysql/conf.d 
            -v /var/mysql/logs:/logs 
            -v /var/mysql/data:/var/lib/mysql 
            -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
      ```

