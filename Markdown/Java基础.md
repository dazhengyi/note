# 集合

## collection

### List

#### 循环删除三种方式

1. for循环删除： list.remove(i)

   弊端：删除一个元素还行，删除多个元素可能会漏删除，因为i一直在递增，当删除了一个元素后集合的size变小，原本的第二个元素跑到了第一个，而比较时是比较下标为i的元素
   解决方法：倒着遍历集合，采用i--的方式，这样集合删除元素虽然会改变集合大小，但是对结果没有影响

   ```java
   for（int i = 0 ; i<list.size();i++）{
            if(list.get(i)==1){
                list.remove(i)
            }   
   }
   ```

2. foreach循环删除 list.remove()  
    弊端：删除一个元素后必须break(),不然会报错，因此也只能删除一个元素

   ```java
   for (String s : list) {
       if (s.length() == 4) {
           list.remove(s);
           break;
       }
   }
   ```

3. ```
   Iterator 迭代器删除（推荐）
          // Role是角色实体类，拥有id属性
          Iterator<Role> roleListIterator = roleList.iterator();
               while (roleListIterator.hasNext()) {
                   Integer roleId = roleListIterator.next().getRoleId();
                   if (roleId == 1 || roleId == 2 || roleId == 3) {
                       roleListIterator.remove();
                   }
               }
    容易踩坑：.next()。在循环中.next()永远指的是下一个元素，而不是当前循环的元素
            比如：
            当第一次进入循环时：
            while (roleListIterator.hasNext()) {
                  // Integer roleId = roleListIterator.next().getRoleId();
                   if (roleListIterator.next()（这是第一个元素） == 1 ||  
                       roleListIterator.next()（这是第二个元素） == 2 || 
                       roleListIterator.next()（这是第三个元素） == 3)   
                   {
                       roleListIterator.remove();
                   }
               }
             如果像上面这样写，那么就会报空指针异常，因为到后面会满足roleListIterator.hasNext()
             而没有.next()可以取值了。所以当钱循环的元素要对比多个值时需要先将其赋值。这样写：  
             Integer roleId = roleListIterator.next().getRoleId();
   ```

​       

​     







